# synshop.info

[![Build Status](https://drone.datavi.be/api/badges/sneak/synshop.info/status.svg)](https://drone.datavi.be/sneak/synshop.info)

Jekyll sources for the [synshop.info](https://synshop.info) website.

# Rationale

The Syn Shop has a lot of information spread across a few different
places/sites, this is a quick simple reference card for the most important
facts, and links to all of those different places.

# See Also

* [jekyll](https://jekyllrb.com/)
* [jekyll/jekyll](https://github.com/jekyll/jekyll)

# Author

[sneak](mailto:sneak@sneak.berlin)

Any errors here are mine, not the Syn Shop's. (Although, if you see any,
[please let me know so I can fix it](mailto:sneak@sneak.berlin).)

# Copyright / License

<a href="http://www.wtfpl.net/"><img
src="http://www.wtfpl.net/wp-content/uploads/2012/12/wtfpl-badge-4.png"
width="80" height="15" alt="WTFPL" /></a>

Released into the public domain under the [WTFPL](https://en.wikipedia.org/wiki/WTFPL).  No rights reserved.
