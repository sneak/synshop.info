export LC_ALL := C.UTF-8
export LANG := en_US.UTF-8
export LANGUAGE := en_US.UTF-8

.PHONY: vendor localserve clean distclean deploy build

default: localserve

build: _site

_includes/gitrev.html:
	git rev-parse HEAD | colrm 9 > $@

_site: vendor _includes/gitrev.html
	JEKYLL_ENV=production bundle exec jekyll build
	find ./_site

vendor:
	bundle install --path vendor/bundle

localserve: vendor _includes/gitrev.html
	JEKYLL_ENV=production bundle exec jekyll serve --incremental

clean:
	rm -rf tmp _site .bundle vendor

distclean: clean
	rm -rfv vendor
