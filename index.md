---
layout: page
---

# Unofficial SYN Shop Info Page

<img src="/s/img/synshop.logo.v4.svg" style="width: 10em">

<br />

<small>Note: This site is a volunteer compilation.  <a
href="https://synshop.org">synshop.org</a> is the official SYN Shop website.
</small>

# SYN Shop

* SYN Shop: The Las Vegas Valley Hackerspace!
    * Located at [5967 Harrison Dr, Las Vegas, NV 89120, USA, Sol III](https://www.google.com/maps/place/5967+Harrison+Dr,+Las+Vegas,+NV+89120/)
        * Note: recently moved!  This is the new/current address.
        * [Move FAQ](https://rtfm.synshop.org/users/SYNShop3.0/FAQ/)
    * [https://synshop.org](https://synshop.org)
    * [info@synshop.org](mailto:info@synshop.org)
    * 501(c)(3) not-for-profit organization
      ([donate!](https://synshop.org/donate))

<iframe width="560" height="315" src="https://www.youtube-nocookie.com/embed/00lU7WQ39vQ" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>

## Connect

* [SYN Shop Discord](https://synshop.org/discord)
* [SYN Shop Twitter (@synshop)](https://twitter.com/synshop)
* [SYN Shop Instagram (@synshoplv)](https://www.instagram.com/synshoplv)
* [SYN Shop GitHub (@synshop)](https://github.com/synshop)
    * RTFM Site [sources](https://github.com/synshop/rtfm) (PRs welcome)
* [SYN Shop Ko-fi](https://ko-fi.com/synshop) (donations)
* [SYN Shop Meetup Page](https://www.meetup.com/synshop)

## General Info

* [FAQ](https://rtfm.synshop.org/users/FAQ/)
* Approximately 120 members as of September 2020
* Membership costs $60 USD per month (required to use the equipment)
* Normally open to the public every day except Sunday
* **Currently not open to non-members due to COVID-19**
    * Otherwise, the normal open hours would be:
        * 18:00-22:00 PST Mon-Fri
        * 15:00-22:00 PST Sat
        * Closed Sun
* Members are currently welcome by appointment with keyholders
* Potential members are also welcome to visit by appointment with keyholders
* All are invited to join!
    * [Sign Up For
  Membership](https://synshop.org/joining-syn-shop)
* [Make a tax-deductible donation to support the SYN Shop!](https://synshop.org/donate)
    * Contact [info@synshop.org](mailto:info@synshop.org) for a donation
      receipt
* [Teach a class at the SYN Shop!](https://synshop.org/teach-class)

# Resources

* [SYN Shop Official Website](https://synshop.org/)
    * Event Listings
    * Contact Form
    * Shop Status
    * Membership Sign Up (membership is open)
    * Account / Payment Settings
    * [Membership Guidelines](https://synshop.org/membership)

<br/>

* [SYN Shop
  Rules](https://rtfm.synshop.org/users/SYN%20Shop%20Rules%20v3%202020-08-12.pdf)
  (version 2020-08-12)

<br/>

* [SYN Shop Manual (RTFM)](https://rtfm.synshop.org/)
    * [Official FAQ](https://rtfm.synshop.org/users/FAQ/)
    * Manual for the shop, includes:
        * [Full Equipment
          List](https://rtfm.synshop.org/users/SYN%20Shop%20Tool%20List/)
            * TL;DR: 3D Printers, Laser Cutters, Lathes, Power Tools, Many
              Saws, Hand Tools, Compressor, Welders, Sewing Machines,
              Soldering Irons, and more!

<br/>

* SYN Shop Discussion Lists
    * Announcements: `synshop-announce`
        * Subscribe: [synshop-announce+subscribe@googlegroups.com](mailto:synshop-announce+subscribe@googlegroups.com)
    * General Discussion: `synshop`
        * Subscribe: [synshop+subscribe@googlegroups.com](mailto:synshop+subscribe@googlegroups.com)

<br/>

* [SYN Shop Podcast](https://www.twitch.tv/synshop)
    * Live on Saturdays at 19:30 (7:30PM) PST!
    * [SYN Shop Podcast Archives](https://www.youtube.com/user/SYNShop)

<br/>

* [SYN Shop Discord Chat](https://synshop.org/discord)
    * [Join Here](https://synshop.org/discord)
    * Most shop members prefer to use this
    * Note that you must also agree to the [Discord Terms Of Service
      (TOS)](https://discord.com/terms), separate from the SYN Shop rules,
      to use this service, and that your PII and direct messages to and from
      other users will be logged by Discord and will *not* remain private.

